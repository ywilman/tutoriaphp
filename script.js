
    $(document).off('click', '.btnCambiarForm');
	$(document).on('click', '.btnCambiarForm', function(event) {
        var destino = $(this).attr("data-destino");
        $.post(destino, { puntos : '../' }, function(data){
            $('#contenido').html(data);
        });
	});

    $(document).off("click",'.btnOperacion');
    $(document).on("click",'.btnOperacion', function(e){
        e.preventDefault();

        var operacion = $(this).attr('data-operacion');	// registrar, eliminar,  modificar o guardar cambios
        var destino = $(this).attr('data-destino');
        var metodo = 'POST';
        var parametros = '';
        var puntos  = '../';

        var textoAlerta;
        if(operacion === "registrar"){
        	var form = $('.FormularioAjax');
            parametros =  form.serialize()
            textoAlerta = "Los datos quedarán almacenados en el sistema";
            textoResultado = 'Registro almacenado con éxito.';

        }else if(operacion === "eliminar"){
            var idRegistro = $(this).attr('id');
            parametros = {'codigo': idRegistro, 'op': 'eliminar', 'puntos': puntos};
            textoAlerta="Los datos serán eliminados completamente del sistema";
            textoResultado = 'Registro eliminado con éxito.';

        }else if(operacion === "modificar"){
            var idRegistro = $(this).attr('id');
            $.post(destino, { 'puntos' : puntos, 'codigo': idRegistro }, function(data){
                $('#divCargarFormulario').html(data);
            });
            return;
        }else if(operacion === "guardarCambios"){
            var form = $('.FormularioAjax');
            parametros =  form.serialize()
            textoAlerta = "El registro será actualizado.";
            textoResultado = 'Registro actualizado con éxito.';
        }else{
            textoAlerta="";
        }

        if (!confirm("¿Está seguro de realizar esta operación? " + textoAlerta)) {
        	return;
        }

        $.ajax({
        	url: destino,
        	type: metodo,
        	data: parametros,
        	// dataType: 'json',
        	cache: false,
        })
        .done(function(result) {
        	if (result == 1) {
                if (operacion == 'registrar') {
                    $.post('vista/test/mostrarTest.php', {}, function(respuesta) {
                        $('#cargarTabla').html(respuesta);
                    });
                    $('#txtNombre').val('');
                    $('#txtDescripcion').val('');
                }

                if (operacion  === 'eliminar') {
                    $("#fila" + idRegistro).remove();
                    $('.RespuestaAjax').html(textoResultado);
                }
                if (operacion == 'guardarCambios') {
                    $.post('vista/test/registrarTest.php', {}, function(respuesta) {
                        $('#divCargarFormulario').html(respuesta);
                    });
                    $.post('vista/test/mostrarTest.php', {}, function(respuesta) {
                        $('#cargarTabla').html(respuesta);
                    });
                    $('.RespuestaAjax').html(textoResultado);
                }
        	} else {
        		alert('Error inesperado, recargue la página y vuelva intentarlo.');
        	}
        })
        .fail(function() {
        	alert('Error inesperado al intentar comunicarse con el servidor.');
        })
    });