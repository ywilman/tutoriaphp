<?php
$_POST['puntos'] = '../../';

require($_POST['puntos']. 'controlador/testControlador.php');
$codigo = $_POST['codigo'];
$datos = CargarPorId($codigo);
?>
<div class="panel panel-warning">
	<div class="panel-heading"> Modificar test  </div>
	<div class="panel-body">

		<form action="#" method="post" name="form" class="FormularioAjax form-horizontal" enctype="multipart/form-data" role="form">
			<input type="hidden" name="puntos" value="../">
			<input type="hidden" name="op" value="guardarCambios">
			<input type="hidden" name="txtCodigo" value="<?php echo $codigo; ?>">
		<div class="form-group">
		    <label for="txtNombre" class="col-lg-2 control-label">Nombre</label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtNombre" name="txtNombre"
		             placeholder="Nombre" value="<?php echo $datos[0]['tes_nombre']; ?>">
		    </div>
		</div>
		  <div class="form-group">
		    <label for="txtDescripcion" class="col-lg-2 control-label">Descripcion </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"
		             placeholder="Descripcion"  value="<?php echo $datos[0]['tes_descripcion']; ?>">
		    </div>
		  </div>

		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      	<button class="btnOperacion btn btn-warning"
					data-operacion="guardarCambios"
					data-destino="controlador/testControlador.php">
					Guardar cambios
				</button>
				<div id="respuestas" class="RespuestaAjax"></div>
		    </div>
		  </div>
		</form>
	</div>
</div>