<?php
	$_POST['puntos'] = '../../';
	require($_POST['puntos']. "controlador/preguntasControlador.php");
	$datos = Cargar();
?>
<script>
  $(function () {
    $('#tablaDatos').DataTable();
  })
</script>

<div class="panel panel-primary">
	<div class="panel-heading"> Lista de preguntas </div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-hover table-condensed " id="tablaDatos">
				<thead>
					<tr>
						<th> </th>
						<th> Test </th>
						<th> Descripcion </th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($datos as $value) { ?>
						<tr id="fila<?php echo $value['tes_codigo']; ?>">
							<td>
								<button class="btnOperacion btn btn-danger"
									data-operacion="eliminar"
									data-destino="controlador/testControlador.php"
									id="<?php echo $value['tes_codigo']; ?>" >
									Elimnar
								</button>
								<button class="btnOperacion btn btn-warning"
									data-operacion="modificar"
									data-destino="vista/test/modificarTest.php"
									id="<?php echo $value['tes_codigo']; ?>" >
									Modificar
								</button>
							</td>
							<td> <?php echo $value['tes_nombre']; ?> </td>
							<td> <?php echo $value['pre_descripcion']; ?> </td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
