<div class="panel panel-primary">
	<div class="panel-heading"> Registro de pregunta </div>
	<div class="panel-body">

		<form action="#" method="post" name="form" class="FormularioAjax form-horizontal" enctype="multipart/form-data" role="form">
			<input type="hidden" name="puntos" value="../">
			<input type="hidden" name="op" value="registrar">
		<div class="form-group">
		    <label for="txtNombre" class="col-lg-2 control-label">Nombre</label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtNombre" name="txtNombre"
		             placeholder="Nombre">
		    </div>
		</div>
		  <div class="form-group">
		    <label for="txtDescripcion" class="col-lg-2 control-label">Descripcion </label>
		    <div class="col-lg-10">
		      <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"
		             placeholder="Descripcion del tes">
		    </div>
		  </div>

		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      	<button class="btnOperacion btn btn-primary"
					data-operacion="registrar"
					data-destino="controlador/testControlador.php">
					Guardar
				</button>
				<div id="respuestas" class="RespuestaAjax"></div>
		    </div>
		  </div>
		</form>
	</div>
</div>