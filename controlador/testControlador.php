<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '';
require($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'registrar') {
	$obj = new Conexion();

	$parametros = [
		":tes_nombre" => $_POST['txtNombre'],
		":tes_descripcion" => $_POST['txtDescripcion']
	];

	$query = 'INSERT INTO tes_test (tes_nombre, tes_descripcion) VALUES (:tes_nombre, :tes_descripcion)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();
	$parametros = [
		":tes_codigo" => $_POST['txtCodigo'],
		":tes_nombre" => $_POST['txtNombre'],
		":tes_descripcion" => $_POST['txtDescripcion']
	];

	$query = "UPDATE tes_test SET tes_nombre = :tes_nombre, tes_descripcion = :tes_descripcion
				WHERE tes_codigo = :tes_codigo";

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminar') {
	$obj = new Conexion();

	$parametros = [
		":tes_codigo" => $_POST['codigo']
	];

	$query = 'DELETE FROM tes_test WHERE tes_codigo = :tes_codigo';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

function CargarTest(){
	$sql = 'SELECT tes_codigo, tes_nombre, tes_descripcion FROM tes_test';
	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql);

	return $datos;
}

function CargarPorId($codigo){
	$parametros = [
		":tes_codigo" => $codigo
	];

	$sql = "SELECT tes_nombre, tes_descripcion
			FROM tes_test
			WHERE tes_codigo = :tes_codigo
			LIMIT 1";

	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql, $parametros);

	return $datos;
}
?>







