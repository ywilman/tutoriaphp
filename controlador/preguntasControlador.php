<?php
$puntos = isset($_POST['puntos']) ? $_POST['puntos'] : '';
require($puntos. 'modelo/conxion.php');

$operacion = isset($_POST['op']) ? $_POST['op'] : '';

if ($operacion == 'registrar') {
	$obj = new Conexion();

	$parametros = [
		":pre_codtes" => $_POST['selTest'],
		":pre_descripcion" => $_POST['txtDescripcion']
	];

	$query = 'INSERT INTO pre_preguntas (pre_codtes, pre_descripcion) VALUES (:pre_codtes, :pre_descripcion)';

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'guardarCambios') {
	$obj = new Conexion();
	$parametros = [
		":pre_codigo" => $_POST['txtCodigo'],
		":pre_codtes" => $_POST['selTest'],
		":pre_descripcion" => $_POST['txtDescripcion']
	];

	$query = "UPDATE pre_preguntas SET pre_codtes = :pre_codtes, pre_descripcion = :pre_descripcion
				WHERE pre_codigo = :pre_codigo";

	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

if ($operacion == 'eliminar') {
	$obj = new Conexion();

	$parametros = [
		":pre_codigo" => $_POST['codigo']
	];

	$query = 'DELETE FROM pre_preguntas WHERE pre_codigo = :pre_codigo';
	echo $obj->Ejecutar($query, $parametros) ? '1' : 'error';
}

function Cargar(){
	$sql = 'SELECT pre_codigo, pre_codtes, pre_descripcion, tes_nombre
	FROM pre_preguntas
	LEFT JOIN tes_test ON tes_codigo = pre_codtes
	';
	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql);

	return $datos;
}

function CargarPorId($codigo){
	$parametros = [
		":pre_codigo" => $codigo
	];

	$sql = "SELECT pre_codtes, pre_descripcion
			FROM pre_preguntas
			WHERE pre_codigo = :pre_codigo
			LIMIT 1";

	$obj = new Conexion();
	$datos = $obj->CargarDatos($sql, $parametros);

	return $datos;
}
?>







